import React, { Component } from 'react';
import { render } from 'react-dom'
import List from './List'

const TitleList = ({switched, list, remove}) => {
  if (list.isEdited) {
    return (
      <input 
        type="text"
        placeholder="Enter your list name"
        ref={input => input && input.focus()}
        onKeyPress={(e) => {
          if (e.charCode === 13) {
            switched(list.id, e.target.value)
          }
        }}
        onBlur={(e) => {
          switched(list.id, e.target.value)
        }}
        />
    )
  } else {
    return (
      <div>
        <span 
          onClick={(e) => {
            e.preventDefault()
            switched(list.id)
          }}>
          {list.name}
        </span>
        <span onClick={(e) => {
            e.preventDefault()
            switched(list.id)
        }}> edit</span>
        <span 
          onClick={()=>{remove(list.id)}}
        > x
        </span>
      </div>
    )
  }
}

const NewList = ({list}) => {
  return(
    <List data={list}
    />
  )
}

const ListsContainer = ({lists, switched, remove}) => {
  const listNode = lists.map((list) => {
    return(
      <div>
        <TitleList
          switched={switched}
          list={list}
          remove={remove}
        />
        <NewList 
          list={list}
          key={list.id}
        />
      </div>
    )
  })

  return(
    <div>
      {listNode}
    </div>
  )
}

const Title = ({none}) => {
  return (
    <h1>HEADER 1</h1>
  )
}

class App extends Component {
  constructor (props) {
    super (props)

    this.state = {
      list: [],
      id: 0,
    }
  }

  addNewList (list) {
    const newList = {
      id: this.state.id++,
      isEdited: false,
      name: 'New list'
    } 

    this.setState((prevState) => {
      list: prevState.list.push(newList)
    })
  }

  handleSwitchTitle (id, newName) {
    this.state.list.forEach((item, i) => {
      if( item.id === id) {
        this.state.list[i].isEdited = !this.state.list[i].isEdited
        this.setState({
          isEdited: !this.state.list[i].isEdited
        })

        if(newName) {
          this.state.list[i].name = newName
          this.setState({
            name: newName
          })
        }
      }
    })  
  }

  handleRemove (id) {
    console.log(this.state.list)
    const remainder = this.state.list.filter((item) => {
      if( item.id !== id) {
        return item
      }
    })
    console.log(remainder)

    this.setState({list: remainder})
  }

  render () {
    return (
      <div>
        <Title />
        <ListsContainer 
          lists={this.state.list}
          switched={this.handleSwitchTitle.bind(this)}
          remove={this.handleRemove.bind(this)}
        />
        <button onClick={this.addNewList.bind(this)}>
          Add TODO List
        </button>
      </div>
    )
  }
}

render (<App />, document.getElementById('root'))
export default App