import React, { Component } from 'react';

const Title = ({counter}) => {
  return(
    <div>
      <div>
        <p className='counter'>{counter}</p>
      </div>
    </div>
  )
}

const TodoForm = ({addTodo}) => {
  let input;
  
  return(
    <div>
      <form onSubmit={(e) => { 
        e.preventDefault()
        addTodo(input.value)
        input.value=''
      }}>
        <input type="text" ref={node => { input = node }}/>
      </form>
    </div>
  )
}

const Todo = ({todo, remove, done, isStrikeThrough}) => {
  return (
    <div>
      <span className="removeElement" onClick={()=>{remove(todo.id)}}>x</span>
      <span 
        onClick={()=>{done(todo.id)}}
        style={isStrikeThrough ? {textDecoration: 'line-through'} : {textDecoration : 'none'}}
      >
        {todo.text}
      </span>
    </div>
  )
}

const TodoList = ({todos, remove, done, isStrikeThrough}) => {
  const todoNode = todos.map((todo) => {
    return (
      <Todo 
        todo={todo} 
        key={todo.id} 
        remove={remove}
        done={done}
        isStrikeThrough={todo.isStrikeThrough}
      />)
  })
  
  return(
    <div>
      {todoNode}
    </div>
  )
}

window.id = 0
export default class List extends Component {
  constructor(props) {

    super(props)
    this.state = {
      data: [],
      id: 0,
    }
  }
 
  addTodo (val) {
    val.trim()
    const todo = { 
      text: val, 
      id: this.state.id++,
      isStrikeThrough: false
    }

    if (val) {
      this.state.data.push(todo)
      this.setState({data: this.state.data})
    }
  }

  handleRemove (id) {
    const remainder = this.state.data.filter((todo) => {
      if( todo.id !== id) {
        return todo
      }
    })

    this.setState({data: remainder})
  }

  handleDone (id) {
    this.state.data.forEach((todo, i) => {
      if( todo.id === id) {
        this.state.data[i].isStrikeThrough = !this.state.data[i].isStrikeThrough
        this.setState({
          isStrikeThrough: !this.state.data[i].isStrikeThrough
        })
      }
    })  
  }

  render() {
    return (
      <div className='List'>
        <Title 
          counter={this.state.data.length}
        />
        <TodoForm 
          addTodo={this.addTodo.bind(this)}
        />
        <TodoList 
          todos={this.state.data}
          remove={this.handleRemove.bind(this)}
          done={this.handleDone.bind(this)}
          isStrikeThrough={this.state.isStrikeThrough}
        />
      </div>
    )
  }
}

// render(<List />, document.getElementById('root'))
// export default List
